// let
// const -> Const é uma variavel que não pode ter re atribuições (reassign)
// var

// string
// number
// boolean
// object
// undefined
// function

document.addEventListener("DOMContentLoaded", () => {
 //
 //
    const btnMagicNumber = document.getElementById("btn-magic-number");
    const inputMagicNumber = document.getElementById("magic-number");
    const attemptEl = document.getElementById("attempt");
    const tryList = document.getElementById("try-list");
    const mainContainer = document.querySelector("body");

    const maxAttempt = 10;
    const minSecretNumber = 1;
    const maxSecretNumber = 100;
    let secretNumber = Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber);
    let attempt = 0;
    console.log(secretNumber)

    attemptEl.innerText = maxAttempt;
     
    btnMagicNumber.addEventListener("click", function(){
        attempt++;
        attemptEl.innerHTML = maxAttempt - attempt;
        const magicNumber = Math.floor(+inputMagicNumber.value);
        inputMagicNumber.value = "";
        inputMagicNumber.focus();
        // Renderizando a lista por completo
        // tryList.innerHTML += `<li>${magicNumber}</li>`; // Interpolacao / Template String
        
        // Renderizando um item apenas.
        const tryItemElement = document.createElement("li");
        tryItemElement.innerHTML = magicNumber;
        if(magicNumber > secretNumber) {
            tryItemElement.classList.add("higher-number");
        } else if (magicNumber < secretNumber) {
            tryItemElement.classList.add("lowest-number");
        } else {
            const winTitle = document.createElement("h2");
            const retryButton = document.createElement("button");

            tryItemElement.classList.add("correct-number");
            winTitle.innerText = "Você venceu!"; 
            retryButton.innerText = "Tentar Novamente";

            retryButton.addEventListener("click", function(){
               winTitle.remove();
               retryButton.remove();
               tryList.innerHTML = "";
               attempt = 0;
               attemptEl.innerHTML = maxAttempt;
               btnMagicNumber.removeAttribute("disabled");
               secretNumber = Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber);     
            })

            mainContainer.append(winTitle);
            mainContainer.append(retryButton);
            btnMagicNumber.setAttribute("disabled", false);

        }

        tryList.append(tryItemElement)
        if(attempt >= 10) {
            btnMagicNumber.setAttribute("disabled", false);
            const lostTitle = document.createElement("h2");
            lostTitle.innerText = "Você perdeu!"; 

            const retryButton = document.createElement("button");
            retryButton.innerText = "Tentar Novamente";
            retryButton.addEventListener("click", function(){
                lostTitle.remove();
                retryButton.remove();
                tryList.innerHTML = "";
                attempt = 0;
                attemptEl.innerHTML = maxAttempt;
                btnMagicNumber.removeAttribute("disabled");
                secretNumber = Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber);     
             })
             
            mainContainer.append(retryButton);
            mainContainer.append(lostTitle);
        }
    });
});




